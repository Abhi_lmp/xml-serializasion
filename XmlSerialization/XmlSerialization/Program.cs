using System.IO;
using System.Xml.Serialization;





namespace XmlSerialization
{
    public class Program
    {
        public static void Main(string[] args)
        {
            

            AddressDetails details = new AddressDetails();
            details.HouseNo = 4;
            details.StreetName = "Rohini";
            details.City = "Delhi";
            Serialize(details);
        }
        static public void Serialize(AddressDetails details)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(AddressDetails));
            using (TextWriter writer = new StreamWriter(@"C:\Xml.xml"))
            {
                serializer.Serialize(writer, details);
            }
        }

      
    }
}
